exports.Start = function (data, done) {
  console.log('Start', 'start!');
  //console.log('data:', data);
  done(data);
}

exports.End = function (data, done) {
  console.log('End', 'start!');
  //console.log('data:', data);
  done(data);
}

exports.defaultErrorHandler = function(error, done) {
  // Called if errors are thrown in the event handlers
  console.log(error);
  done();
};

exports.onBeginHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('----------------------');
  console.log('onBeginHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  done(data);
};

exports.onEndHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('onEndHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  console.log('----------------------');
  done(data);
};

exports.Apply = function (data, done) {
  console.log('Apply', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.ApplyDone = function (data, done) {
  console.log('ApplyDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Department_Approve = function (data, done) {
  console.log('Department Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Department_ApproveDone = function (data, done) {
  console.log('Department ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Deputy_Manager_Approve = function (data, done) {
  console.log('Deputy Manager Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Deputy_Manager_ApproveDone = function (data, done) {
  console.log('Deputy Manager ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_Approve = function (data, done) {
  console.log('Equipment Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_ApproveDone = function (data, done) {
  console.log('Equipment ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_Approve = function (data, done) {
  console.log('Administration Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_ApproveDone = function (data, done) {
  console.log('Administration ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_Deputy_Manager_Approve = function (data, done) {
  console.log('Equipment Deputy Manager Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_Deputy_Manager_ApproveDone = function (data, done) {
  console.log('Equipment Deputy Manager ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_Deputy_Manager_Approve = function (data, done) {
  console.log('Administration Deputy Manager Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_Deputy_Manager_ApproveDone = function (data, done) {
  console.log('Administration Deputy Manager ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Rotating_President_Approve = function (data, done) {
  console.log('Rotating President Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Rotating_President_ApproveDone = function (data, done) {
  console.log('Rotating President ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.President_Approve = function (data, done) {
  console.log('President Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.President_ApproveDone = function (data, done) {
  console.log('President ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Finance_Manager_Approve = function (data, done) {
  console.log('Finance Manager Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Finance_Manager_ApproveDone = function (data, done) {
  console.log('Finance Manager ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Purchase = function (data, done) {
  console.log('Purchase', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.PurchaseDone = function (data, done) {
  console.log('PurchaseDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Fetch_From_Equipment = function (data, done) {
  console.log('Fetch From Equipment', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Fetch_From_EquipmentDone = function (data, done) {
  console.log('Fetch From EquipmentDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Fetch_From_Administration = function (data, done) {
  console.log('Fetch From Administration', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Fetch_From_AdministrationDone = function (data, done) {
  console.log('Fetch From AdministrationDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Deputy_Manager_Approved = function (data, done) {
  console.log('Deputy Manager Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Deputy_Manager_Approved$No = function (data) {
  console.log('Deputy Manager Approved_No', 'start!');
  //console.log('data:', data);
  return data.a;
};

exports.Deputy_Manager_Approved$Is_Equipment = function (data) {
  console.log('Deputy Manager Approved_Is Equipment', 'start!');
  //console.log('data:', data);
  return data.b;
};

exports.Deputy_Manager_Approved$Is_Administration = function (data) {
  console.log('Deputy Manager Approved_Is Administration', 'start!');
  //console.log('data:', data);
  return data.c;
};

exports.Equipment_Approved = function (data, done) {
  console.log('Equipment Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_Approved$Lt5w = function (data) {
  console.log('Equipment Approved_Lt5w', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Equipment_Approved$Lt10w = function (data) {
  console.log('Equipment Approved_Lt10w', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Equipment_Approved$Gte10w = function (data) {
  console.log('Equipment Approved_Gte10w', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Equipment_Approved$Fetch = function (data) {
  console.log('Equipment Approved_Fetch', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Administration_Approved = function (data, done) {
  console.log('Administration Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_Approved$Lt5w = function (data) {
  console.log('Administration Approved_Lt5w', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Administration_Approved$Lt10w = function (data) {
  console.log('Administration Approved_Lt10w', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Administration_Approved$Gte10w = function (data) {
  console.log('Administration Approved_Gte10w', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Administration_Approved$Fetch = function (data) {
  console.log('Administration Approved_Fetch', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Department_Approved = function (data, done) {
  console.log('Department Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Department_Approved$No = function (data) {
  console.log('Department Approved_No', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Department_Approved$Yes = function (data) {
  console.log('Department Approved_Yes', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Administration_Deputy_Manager_Approved = function (data, done) {
  console.log('Administration Deputy Manager Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Administration_Deputy_Manager_Approved$No = function (data) {
  console.log('Administration Deputy Manager Approved_No', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Administration_Deputy_Manager_Approved$Yes = function (data) {
  console.log('Administration Deputy Manager Approved_Yes', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Equipment_Deputy_Manager_Approved = function (data, done) {
  console.log('Equipment Deputy Manager Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Equipment_Deputy_Manager_Approved$No = function (data) {
  console.log('Equipment Deputy Manager Approved_No', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Equipment_Deputy_Manager_Approved$Yes = function (data) {
  console.log('Equipment Deputy Manager Approved_Yes', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Finance_Manager_Approved = function (data, done) {
  console.log('Finance Manager Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Finance_Manager_Approved$No = function (data) {
  console.log('Finance Manager Approved_No', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Finance_Manager_Approved$Yes = function (data) {
  console.log('Finance Manager Approved_Yes', 'start!');
  //console.log('data:', data);
  return false;
};

exports.President_Approved = function (data, done) {
  console.log('President Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.President_Approved$No = function (data) {
  console.log('President Approved_No', 'start!');
  //console.log('data:', data);
  return true;
};

exports.President_Approved$Yes = function (data) {
  console.log('President Approved_Yes', 'start!');
  //console.log('data:', data);
  return false;
};

exports.Rotating_President_Approved = function (data, done) {
  console.log('Rotating President Approved', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Rotating_President_Approved$No = function (data) {
  console.log('Rotating President Approved_No', 'start!');
  //console.log('data:', data);
  return true;
};

exports.Rotating_President_Approved$Yes = function (data) {
  console.log('Rotating President Approved_Yes', 'start!');
  //console.log('data:', data);
  return false;
};

