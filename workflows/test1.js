exports.Start = function (data, done) {
  console.log('Start', 'start!');
  //console.log('data:', data);
  done(data);
}

exports.End = function (data, done) {
  console.log('End', 'start!');
  //console.log('data:', data);
  done(data);
}

exports.defaultErrorHandler = function(error, done) {
  // Called if errors are thrown in the event handlers
  console.log(error);
  done();
};

exports.onBeginHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('----------------------');
  console.log('onBeginHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  done(data);
};

exports.onEndHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('onEndHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  console.log('----------------------');
  done(data);
};

exports.Apply = function (data, done) {
  console.log('Apply', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.ApplyDone = function (data, done) {
  console.log('ApplyDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Rollback = function (data, done) {
  console.log('Rollback', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.RollbackDone = function (data, done) {
  console.log('RollbackDone', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.Approve = function (data, done) {
  console.log('Approve', 'start!');
  //console.log('data:', data);
  done(data);
};

exports.ApproveDone = function (data, done) {
  console.log('ApproveDone', 'start!');
  //console.log('data:', data);
  done(data);
};

