exports.applyDone = function(data, done) {
    this.setProperty('apply', data);
    done();
};

exports.managerDone = function(data, done) {
    this.setProperty('manager', data);
    done();
};

exports.bossDone = function(data, done) {
    this.setProperty('boss', data);
    done();
};

exports.buyDone = function(data, done) {
    this.setProperty('buy', data);
    done();
};

exports.is_manager_approved = function(data, done) {
    done(data);
};

exports.is_manager_approved$yes = function(data, done) {
    const manager = this.getProperty('manager');
    return manager.approved;
};

exports.is_manager_approved$no = function(data, done) {
    const manager = this.getProperty('manager');
    return !manager.approved;
};

exports.is_boss_approved = function(data, done) {
    done(data);
};

exports.is_boss_approved$yes = function(data, done) {
    const boss = this.getProperty('boss');
    return boss.approved;
};

exports.is_boss_approved$no = function(data, done) {
    const boss = this.getProperty('boss');
    return !boss.approved;
};

exports.is_manager = function(data, done) {
    done(data);
};

exports.is_manager$yes = function(data, done) {
    return false;
};

exports.is_manager$no = function(data, done) {
    return true;
};

exports.is_gt_3 = function(data, done) {
    done(data)
};

exports.is_gt_3$yes = function(data, done) {
    const apply = this.getProperty('apply');
    return apply.discount > 3;
};

exports.is_gt_3$no = function(data, done) {
    const apply = this.getProperty('apply');
    return apply.discount <= 3;
};

