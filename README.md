# Workflow processor test project

## Setup
```
> cp config.example.json config.json
> vim config.json
> yarn / npm i
```

## Setup Database
```
> mongo
> use wfp
> db.createUser({ user: 'wfp', pwd: 'wfp', roles: [ { role: 'readWrite', db: 'wfp' } ] })
```

## Generate BPMN handler.js file
```
> cd converter
> node main ../workflows/your_file_name.bpmn
```

## Run in development mode
```
> TODO
```

## Run in production mode
```
> TODO
```

