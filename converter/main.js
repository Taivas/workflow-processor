const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');

const file = path.basename(process.argv[2]);
const basename = path.basename(process.argv[2], '.bpmn');
const dirname = path.dirname(process.argv[2]);

const data = fs.readFileSync(dirname + '/' + file, { xmlMode: true });
const $ = cheerio.load(data.toString('utf-8'));

function underscore (s) {
    return s.replace(/ /g, '_');
}

//const funcTpl1 = `function (data, done) {
//    done(data);
//}`;

//const funcTpl2 = `function (data) {
//    return true;
//}`;

//const funcTpl3 = `function (data) {
//    return false;
//}`;

const fnTplGen = function (fnName, fnType) {
  const fnTpl = {
    task: `function (data, done) {
  console.log('${fnName}', 'start!');
  //console.log('data:', data);
  done(data);
}`,
    exGateY: `function (data) {
  console.log('${fnName}', 'start!');
  //console.log('data:', data);
  return true;
}`,
    exGateN: `function (data) {
  console.log('${fnName}', 'start!');
  //console.log('data:', data);
  return false;
}`
  };
  fnTpl.exGate = fnTpl.task;
  return fnTpl[fnType];
}

let str = '';
str += `exports.Start = ${fnTplGen('Start', 'task')}\n\n`
str += `exports.End = ${fnTplGen('End', 'task')}\n\n`
str += `exports.defaultErrorHandler = function(error, done) {
  // Called if errors are thrown in the event handlers
  console.log(error);
  done();
};\n\n`
str += `exports.onBeginHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('----------------------');
  console.log('onBeginHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  done(data);
};\n\n`

str += `exports.onEndHandler = function(currentFlowObjectName, data, done) {
  // do something
  console.log('onEndHandler');
  console.log('currentFlowObjectName:', currentFlowObjectName);
  console.log('data:', data);
  console.log('----------------------');
  done(data);
};\n\n`

$('bpmn\\:task').each((index, el) => {
    const fnName = el.attribs.name;
    str += `exports.${underscore(fnName)} = ${fnTplGen(fnName, 'task')};\n\n`;
    str += `exports.${underscore(fnName)}Done = ${fnTplGen(fnName + 'Done', 'task')};\n\n`;
});

$('bpmn\\:exclusiveGateway').each((index, el) => {
    const fnName = el.attribs.name;
    str += `exports.${underscore(fnName)} = ${fnTplGen(fnName, 'exGate')};\n\n`;
    $(`bpmn\\:sequenceFlow[sourceRef='${el.attribs.id}']`).each((index, flowEl) => {
        const flowName = flowEl.attribs.name;
        str += `exports.${underscore(fnName)}$${underscore(flowName)} = ${index === 0 ? fnTplGen(fnName + '_' + flowName, 'exGateY') : fnTplGen(fnName + '_' + flowName, 'exGateN')};\n\n`;
    })
});
fs.writeFileSync(dirname + '/' + `${basename}.js`, str);
