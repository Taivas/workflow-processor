// entry of program

import fs from 'fs';
import bpmn from 'bpmn';
import path from 'path';
import uuidV4 from 'uuid/v4';
import config from './config.json'
console.log(config);

const insId = uuidV4();

const w = function PromiseWrapper(fn) {
  return function() {
    return new Promise((resolve, reject) => {
      fn(arguments, function() {
        resolve(arguments);
      });
    })
  };
}

const manager = new bpmn.ProcessManager({
  persistencyOptions: {
    uri: config.mongodb.uri
  }
});
manager.addBpmnFilePath(path.resolve('./workflows/2_1.bpmn'));

manager.createProcess(insId, (err, myProcess) => {
  console.log(err, myProcess);
  console.log('insId', insId);
  if(!myProcess || err) {
    console.log('load process failed, err:', err);
  } else {
    fs.readFile('./workflows/2_1_result.json', 'utf8', async (err, jsData) => {
      if(err) {
        console.log('readFile failed:', err);
      } else {
        try {
          //console.log(jsData);
          await myProcess.triggerEvent('Start', jsData.ApplySheet);
          //await myProcess.taskDone('Apply', true); 
          //await myProcess.taskDone('Department Approve', true); 
          const state = await myProcess.getState();
          console.log('current state:', state);
          const history = await myProcess.getHistory();
          console.log('history:', history);
          //await myProcess.triggerEvent('Deputy_Manager_Approve', true); 
        } catch(e) {
          console.log('bpmn process error:', e);
        }
      }
    });
  }
});

